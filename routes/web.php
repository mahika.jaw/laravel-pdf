<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\InvoiceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
});


Route::get('/login', function () {
    return view('login');
});

Route::get('/register', function () {
    return view('register');
});

Route::get('/logout', [App\Http\Controllers\HomeController::class, 'logout'])->name('logout');


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('dashboard');
// Route::get('/dashboard', function () {
//     return view('dashboard');
// });

Route::resource('/products', ProductController::class);
Route::resource('/customers', CustomerController::class);
//Route::get('get-customers', [CustomerController::class, 'getCustomers'])->name('get-customers');
Route::resource('/invoices', InvoiceController::class);
Route::post('/viewinvoice', [InvoiceController::class, 'viewinvoice'])->name('view');
Route::get('/viewpage', function(){
    return view('invoices.view');
})->name('viewpage');
Route::post('/getCustomers', [InvoiceController::class, 'getCustomers'])->name('getCustomers');
Route::post('/getProducts', [InvoiceController::class, 'getProducts'])->name('getProducts');

Route::get('downloadpdf', [InvoiceController::class, 'pdfview'])->name('downloadpdf');
Route::get('invoices/pdf/{id}', [InvoiceController::class, 'pdfview']);
