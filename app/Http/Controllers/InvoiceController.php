<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Invoice_item;
use App\Models\Customer;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use PDF;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            //$invoices = Invoice::all();
            $invoices = Invoice::select('*', 'invoices.id as idp')->join('customers', 'customers.id', '=', 'invoices.customer_id')->orderBy('idp','asc')->get();  

            return datatables()->of($invoices)
                ->addColumn('action', function ($row) {
                    //dd($row);
                    $html = '<a data-rowid="' . $row->idp . '" href="invoices/'. $row->idp .'" class="btn btn-xs btn-info btn-view">View</a> ';
                    $html .= '<a data-rowid="' . $row->id . '" href="invoices/pdf/'. $row->idp .'" class="btn btn-xs btn-success btn-delete">PDF</a>';
                    return $html;
                })->toJson();
        }

        return view('invoices.index');
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('invoices.create');
    }

    public function getCustomers(Request $request)
    {
        $search = $request->search;

        if($search == ''){
            $customers = Customer::orderby('first_name','asc')
                                    ->select('id','first_name', 'last_name', 'email', 'phone', 'address', 'zipcode')
                                    ->limit(5)
                                    ->get();
        }else{
            $customers = Customer::orderby('first_name','asc')
                                    ->select('id','first_name', 'last_name', 'email', 'phone', 'address', 'zipcode')
                                    ->where('first_name', 'like', '%' .$search . '%')
                                    ->limit(5)
                                    ->get();
        }

        $response = array();
        foreach($customers as $customer){
            $response[] = array(
                "value" => $customer->id,
                "last_name" => $customer->last_name,
                "email" => $customer->email,
                "phone" => $customer->phone,
                "address" => $customer->address,
                "zipcode" => $customer->zipcode,
                "label" => $customer->first_name
            );
        }

        return response()->json($response); 

    }


    public function getProducts(Request $request)
    {
        $search = $request->search;

        if($search == ''){
            $products = Product::orderby('product_title','asc')
                                    ->select('id','product_title', 'price', 'stock', 'sku', 'description', 'category')
                                    ->limit(5)
                                    ->get();
        }else{
            $products = Product::orderby('product_title','asc')
                                    ->select('id','product_title', 'price', 'stock', 'sku', 'description', 'category')
                                    ->where('product_title', 'like', '%' .$search . '%')
                                    ->limit(5)
                                    ->get();
        }

        $response = array();
        foreach($products as $product){
            $response[] = array(
                "value" => $product->id,
                "stock" => $product->stock,
                "price" => $product->price,
                "sku" => $product->sku,
                "category" => $product->category,
                "description" => $product->description,
                "label" => $product->product_title
            );
        }

        return response()->json($response); 

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // do validation
        $data =[
            'invoice_number' =>$request->invoice_number,
            'invoice_date' =>$request->invoice_date,
            'total_value' =>$request->sub_total,
            'customer_id' =>$request->customer_id,
            'created_by' =>Auth::user()->name
        ];
     //dd($data);
        // dd($request->all());
        $invoice_id = Invoice::create($data);
        foreach($request->items as $val){
            //dd($val['product_id']);
        $data_i=[
        'invoice_id' => $invoice_id->id,
        'product_id' =>$val['product_id'],
        'quantity' =>$val['quantity'],
        'cost_price' =>$val['price'],
        'total_price' =>$val['total_price'],
        'created_by' =>Auth::user()->name,
        ];
        //dd($data_i);
        Invoice_item::create($data_i);

        }
        return ['success' => true, 'message' => 'Inserted Successfully'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //dd($id);
        $invoices = Invoice::findorfail($id);  
            //$invoices = Invoice::findorfail('26');  
            $invoice = $invoices;
            $items = $invoices->invoice_items;
            $customer = $invoices->customer;
            //$products = $invoices->invoice_items[0]->products;  
            //dd($invoices->customer);
            //dd($request->rowid);
            //dd($invoices->customer);
            // dd($invoices->invoice_items);
            return view('invoices.view', compact('invoices', 'items', 'customer'));
        // $invoices = Invoice::findorfail($request->rowid);
        // return view('invoices.view', compact('invoices'));
    }

    public function pdfview(Request $request)
    {
        $invoices = Invoice::findorfail('26');
        $invoice = $invoices;
        $items = $invoices->invoice_items;
        $customer = $invoices->customer;
        
        // if($request->has('download')) {
            $pdf = PDF::loadView('invoices.pdftest', compact('invoices', 'items', 'customer'));

            return $pdf->download('invoice_pdf.pdf');
            //return $pdf->download('invoice_pdf.pdf');
            //dd('hello');
            // }
        // return view('invoices.view', compact('invoices', 'items', 'customer'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // do validation
        Invoice::find($id)->update(request()->all());
        return ['success' => true, 'message' => 'Updated Successfully'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
