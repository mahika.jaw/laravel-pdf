@extends('layouts.main')

@section('body')

<style type="text/css">
.parsley-errors-list { 
list-style-type: none
}

.parsley-custom-error-message  {
color: red;
margin-top: 5px;
/* margin-left: -40px; */
}


input.parsley-success,
       select.parsley-success,
       textarea.parsley-success {
         color: #468847;
         background-color: #DFF0D8;
         border: 1px solid #D6E9C6;
       }

       input.parsley-error,
       select.parsley-error,
       textarea.parsley-error {
         color: #B94A48;
         background-color: #F2DEDE;
         border: 1px solid #EED3D7;
       }

       .parsley-errors-list {
         margin: 2px 0 3px;
         padding: 0;
         list-style-type: none;
         font-size: 0.9em;
         line-height: 0.9em;
         opacity: 0;

         transition: all .3s ease-in;
         -o-transition: all .3s ease-in;
         -moz-transition: all .3s ease-in;
         -webkit-transition: all .3s ease-in;
       }

       .parsley-errors-list.filled {
         opacity: 1;
       }

.parsley-type,
       .parsley-required,
       .parsley-equalto,
       .parsley-pattern,
       .parsley-urlstrict,
       .parsley-length,
       .parsley-checkemail{
        color:#ff0000;
       }
/* .toast-success{
 background :#006600;
} */

    .toast-success{background: #006600;}
    .toast-error{background: red;}
    .toast-warning{background: coral;}
    .toast-info{background: cornflowerblue;}
    .toast-question{background: grey;}
</style>



<div class="body flex-grow-1 px-3">
    <div class="container-lg">


            <div class="card mb-4">
                <div class="card-header"><strong>Customer</strong><span class="small ms-1">Database</span></div>
                <div class="card-body">
                <button type="button" style="float: right;" class="btn btn-xs btn-success float-right add">Add Customer</button>
</br>
                <div class="example">
                    <div class="tab-content rounded-bottom">
                        <div class="tab-pane p-3 active preview" role="tabpanel" id="preview-1000">
                            
                            <table id="customers" class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Address</th>
                                        <th>Zipcode</th>
                                        <th width="120px">Action</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            
       


            <!--  -->
            <div class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <form class="form" action="" method="POST">
                        @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">New Customer</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" name="id">
                                
                                <div class="row g-3">
                                    <div class="row g-3">
                                        <div class="col-sm">
                                            <label for="basic-url" class="form-label">First Name</label>
                                            <input type="text" class="form-control form-control-sm" placeholder="First Name"  data-parsley-required="true" data-parsley-error-message="Enter First Name" name="first_name">
                                        </div>
                                        <div class="col-sm">
                                            <label for="basic-url" class="form-label">Last Name</label>
                                            <input type="text" class="form-control form-control-sm" placeholder="Last Name"  data-parsley-required="true" data-parsley-error-message="Enter Last Name" name="last_name">
                                        </div>
                                    </div>

                                    <div class="row g-3">
                                        <div class="col-sm">
                                            <label for="basic-url" class="form-label">Email</label>
                                            <input type="email" class="form-control form-control-sm" placeholder="Enter Email"  data-parsley-required="true" data-parsley-type="email" data-parsley-checkemail data-parsley-checkemail-message="Email Address already Exists" data-parsley-error-message="Enter a valid email" name="email">
                                            
                                        </div>
                                        <div class="col-sm">
                                            <label for="basic-url" class="form-label">Phone</label>
                                            <input type="text" class="form-control form-control-sm" data-parsley-required="true" data-parsley-error-message="Enter valid phone number" data-parsley-type="digits" minlength="10" maxlength="11" placeholder="Phone" name="phone">
                                        </div>
                                    </div>

                                    <div class="row g-3">
                                        <div class="col-sm">
                                            <label for="basic-url" class="form-label">Address</label>
                                            <input type="text" class="form-control form-control-sm" placeholder="Address"  data-parsley-required="true" data-parsley-error-message="Enter the Address" name="address">
                                        </div>
                                        <div class="col-sm">
                                            <label for="basic-url" class="form-label">Zipcode</label>
                                            <input type="text" class="form-control form-control-sm" placeholder="Zipcode" data-parsley-required="true" data-parsley-error-message="Enter the Zipcode" data-parsley-type="digits" minlength="6" maxlength="8" name="zipcode">
                                        </div>
                                    </div>
                                </div>

                        </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary btn-save">Save</button>
                                <button type="button" class="btn btn-primary btn-update">Update</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--  -->

            <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
            <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" ></script>
            <!--js code here-->
            <script>
                $(document).ready(function() {
                    $.noConflict();
                    var token = ''
                    var modal = $('.modal')
                    var form = $('.form')
                    var btnAdd = $('.add'),
                        btnSave = $('.btn-save'),
                        btnUpdate = $('.btn-update');
                    
                    var table = $('#customers').DataTable({
                            ajax: '',
                            serverSide: true,
                            processing: true,
                            aaSorting:[[0,"asc"]],
                            columns: [
                                {data: 'id', name: 'id'},
                                {data: 'first_name', name: 'first_name'},
                                {data: 'last_name', name: 'last_name'},
                                {data: 'email', name: 'email'},
                                {data: 'phone', name: 'phone'},
                                {data: 'address', name: 'address'},
                                {data: 'zipcode', name: 'zipcode'},
                                {data: 'action', name: 'action'},
                            ],
                            "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                                $("td:first", nRow).html(iDisplayIndex +1);
                            return nRow;
                            },
                        });

                        // {
                        //     $("input").removeClass("parsley-error");
                        //     $(".parsley-custom-error-message").text("");
                        // };

                    btnAdd.click(function(){
                        modal.modal()
                        form.trigger('reset')
                        $("input").removeClass("parsley-error");
                        $(".parsley-custom-error-message").text("");
                        modal.find('.modal-title').text('Add New')
                        btnSave.show();
                        btnUpdate.hide()
                    })

                    btnSave.click(function(e){
                        e.preventDefault();
                        if($(".form").parsley().validate()){
                        var data = form.serialize()
                        console.log(data)
                        $.ajax({
                            // headers: {
                            //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('body')
                            // }
                            type: "POST",
                            url: "{{ route('customers.store') }}",
                            data: data,
                            success: function (data) {
                                console.log('hii');
                                if (data.success) {
                                    table.draw();
                                    form.trigger("reset");
                                    modal.modal('hide');
                                    toastr.success('Customer created successfully!')
                                }
                                else {
                                    toastr.error('Something went wrong, try again in some time!')
                                }
                            }
                        }); 
                    }})

                
                    $(document).on('click','.btn-edit',function(){
                        btnSave.hide();
                        btnUpdate.show();
                        $("input").removeClass("parsley-error");
                        $("input").removeClass("parsley-success");
                        $(".parsley-custom-error-message").text("");
                        
                        modal.find('.modal-title').text('Update Record')
                        modal.find('.modal-footer button[type="submit"]').text('Update')

                        var rowData =  table.row($(this).parents('tr')).data()
                        
                        form.find('input[name="id"]').val(rowData.id)
                        form.find('input[name="first_name"]').val(rowData.first_name)
                        form.find('input[name="last_name"]').val(rowData.last_name)
                        form.find('input[name="email"]').val(rowData.email)
                        form.find('input[name="phone"]').val(rowData.phone)
                        form.find('input[name="address"]').val(rowData.address)
                        form.find('input[name="zipcode"]').val(rowData.zipcode)

                        modal.modal()
                    })

                    btnUpdate.click(function(){
                        //if(!confirm("Are you sure?")) return;
                        if($(".form").parsley().validate()){
                        var formData = form.serialize();
                        var updateId = form.find('input[name="id"]').val();
                        $.ajax({
                            type: "PUT",
                            url: "{{'customers/'}}" + updateId,
                            data: formData,
                            success: function (data) {
                                if (data.success) {
                                    table.draw();
                                    modal.modal('hide');
                                    toastr.success('Customer Updated Successfully');
                                }else{
                                    toastr.error('Something went wrong, try again in some time!')
                                }
                            }
                        }); //end ajax
                        }})
                        

                    $(document).on('click','.btn-delete',function(){
                        if(!confirm("Are you sure?")) return;

                        var rowid = $(this).data('rowid')
                        var el = $(this)
                        if(!rowid) return;

                        
                        $.ajax({
                            type: "POST",
                            dataType: 'JSON',
                            url: "{{'customers/'}}" + rowid,
                            data: {_method: 'delete',
                                _token: "{{ csrf_token() }}",
                            },
                            success: function (data) {
                                if (data.success) {
                                    table.row(el.parents('tr'))
                                        .remove()
                                        .draw();
                                        toastr.success('Customer Deleted Successfully');
                                }else{
                                    toastr.error('Something went wrong, try again in some time!')
                                }
                            }
                        }); //end ajax
                    })
                })
            </script>

            <script type="text/javascript">
            

                // Toast Type
                    $('#success').click(function(event) {
                        toastr.success('Record Added Successfully');
                    });
                    $('#info').click(function(event) {
                        toastr.info('You clicked Info toast')
                    });
                    $('#error').click(function(event) {
                        toastr.error('You clicked Error Toast')
                    });
                    $('#warning').click(function(event) {
                        toastr.warning('You clicked Warning Toast')
                    });

                // Toast Image and Progress Bar
                    $('#image').click(function(event) {
                        toastr.options.progressBar = true,
                        toastr.info('<img src="https://image.flaticon.com/icons/svg/34/34579.svg" style="width:150px;">', 'Toast Image')
                    });


                // Toast Position
                    $('#position').click(function(event) {
                        var pos = $('input[name=position]:checked', '#positionForm').val();
                        toastr.options.positionClass = "toast-" + pos;
                        toastr.options.preventDuplicates = false;
                        toastr.info('This sample position', 'Toast Position')
                    });
            </script>
    </div>
</div>




@endsection