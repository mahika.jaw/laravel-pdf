<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>@yield('title', 'Custom Auth Laravel')</title>
    @include('layouts.components.head')
    
  </head>
  
  <body>
  @include('layouts.components.sidebar')
  @include('layouts.components.header')
  @yield('body')
  @include('layouts.components.footer')
  
  </body>
</html>