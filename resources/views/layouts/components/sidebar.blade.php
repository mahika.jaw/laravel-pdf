<div class="sidebar sidebar-dark sidebar-fixed" id="sidebar">
      <div class="sidebar-brand d-none d-md-flex">
        <svg class="sidebar-brand-full" width="118" height="46" alt="CoreUI Logo">
          <use xlink:href="{{asset('dist/assets/brand/coreui.svg#full')}}"></use>
        </svg>
        <svg class="sidebar-brand-narrow" width="46" height="46" alt="CoreUI Logo">
          <use xlink:href="{{asset('dist/assets/brand/coreui.svg#signet')}}"></use>
        </svg>
      </div>
      <ul class="sidebar-nav" data-coreui="navigation" data-simplebar="">
        
        <li class="nav-title">Components</li>
        <li class="nav-item"><a class="nav-link" href="{{route('home')}}">
            <svg class="nav-icon">
              <use xlink:href="{{asset('dist/vendors/@coreui/icons/svg/free.svg#cil-home')}}"></use>
            </svg>Home</a></li>
        <!-- <li class="nav-item"><a class="nav-link" href="{{route('login')}}">
            <svg class="nav-icon">
              <use xlink:href="{{asset('dist/vendors/@coreui/icons/svg/free.svg#cil-drop')}}"></use>
            </svg> Login</a></li>
        <li class="nav-item"><a class="nav-link" href="{{route('register')}}">
            <svg class="nav-icon">
              <use xlink:href="{{asset('dist/vendors/@coreui/icons/svg/free.svg#cil-pencil')}}"></use>
            </svg>Register</a></li> -->

            <li class="nav-item"><a class="nav-link" href="{{route('dashboard')}}">
            <svg class="nav-icon">
              <use xlink:href="{{asset('dist/vendors/@coreui/icons/svg/free.svg#cil-clipboard')}}"></use>
            </svg>Dashboard</a></li>

            <li class="nav-item"><a class="nav-link" href="{{ route('products.index') }}">
            <svg class="nav-icon">
              <use xlink:href="{{asset('dist/vendors/@coreui/icons/svg/free.svg#cil-clipboard')}}"></use>
            </svg>Products</a></li>
        
            <li class="nav-item"><a class="nav-link" href="{{ route('customers.index') }}">
            <svg class="nav-icon">
              <use xlink:href="{{asset('dist/vendors/@coreui/icons/svg/free.svg#cil-user')}}"></use>
            </svg>Customers</a></li>

            <li class="nav-item"><a class="nav-link" href="{{ route('invoices.index') }}">
            <svg class="nav-icon">
              <use xlink:href="{{asset('dist/vendors/@coreui/icons/svg/free.svg#cil-user')}}"></use>
            </svg>Invoices</a></li>
        
      </ul>
      <button class="sidebar-toggler" type="button" data-coreui-toggle="unfoldable"></button>
    </div>