
<div class="wrapper d-flex flex-column min-vh-100 bg-light">
      <header class="header header-sticky mb-4">
        <div class="container-fluid">
          <button class="header-toggler px-md-0 me-md-3" type="button" onclick="coreui.Sidebar.getInstance(document.querySelector('#sidebar')).toggle()">
            <svg class="icon icon-lg">
              <use xlink:href="{{asset('dist/vendors/@coreui/icons/svg/free.svg#cil-menu')}}"></use>
            </svg>
          </button><a class="header-brand d-md-none" href="{{asset('dist/#')}}">
            <svg width="118" height="46" alt="CoreUI Logo">
              <use xlink:href="{{asset('dist/assets/brand/coreui.svg#full')}}"></use>
            </svg></a>
          <!-- <ul class="header-nav d-none d-md-flex">
            <li class="nav-item"><a class="nav-link" href="/">Dashboard</a></li>
            <li class="nav-item"><a class="nav-link" href="dist/#">Users</a></li>
            <li class="nav-item"><a class="nav-link" href="dist/#">Settings</a></li>
          </ul>
          <ul class="header-nav ms-auto">
            <li class="nav-item"><a class="nav-link" href="dist/#">
                <svg class="icon icon-lg">
                  <use xlink:href="dist/vendors/@coreui/icons/svg/free.svg#cil-bell"></use>
                </svg></a></li>
            <li class="nav-item"><a class="nav-link" href="dist/#">
                <svg class="icon icon-lg">
                  <use xlink:href="dist/vendors/@coreui/icons/svg/free.svg#cil-list-rich"></use>
                </svg></a></li>
            <li class="nav-item"><a class="nav-link" href="dist/#">
                <svg class="icon icon-lg">
                  <use xlink:href="dist/vendors/@coreui/icons/svg/free.svg#cil-envelope-open"></use>
                </svg></a></li>
          </ul> -->
          <ul class="header-nav ms-3">
            <li class="nav-item">{{ Auth::user()->name }}</li>
            <li class="nav-item dropdown"><a class="nav-link py-0" data-coreui-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <div class="avatar avatar-md"><img class="avatar-img" src="{{asset('dist/assets/img/full.jpg')}}" style="height: 100%;" alt="user@email.com"></div>
              </a>
              <div class="dropdown-menu dropdown-menu-end pt-0">
               

                  <a class="dropdown-item" href="{{route('logout')}}">
                  <svg class="icon me-2">
                    <use xlink:href="{{asset('dist/vendors/@coreui/icons/svg/free.svg#cil-account-logout')}}"></use>
                  </svg> Logout</a>
              </div>
            </li>
          </ul>
        </div>
        <!-- <div class="header-divider"></div> -->
        <!-- <div class="container-fluid"> -->
          <!-- <nav aria-label="breadcrumb">
            <ol class="breadcrumb my-0 ms-2">
              <li class="breadcrumb-item"> -->
                <!-- if breadcrumb is single-->
                <!-- <span>Home</span>
              </li>
              <li class="breadcrumb-item active"><span>Blank</span></li>
            </ol>
          </nav> -->
        <!-- </div> -->
      </header>