<div class="body flex-grow-1 px-3">
        <div class="container-lg">
        </div>
      </div>
      <footer class="footer">
        <div><a href="https://coreui.io">CoreUI </a><a href="https://coreui.io">Bootstrap Admin Template</a> © 2023 creativeLabs.</div>
        <div class="ms-auto">Powered by&nbsp;<a href="https://coreui.io/docs/">CoreUI UI Components</a></div>
      </footer>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="{{asset('dist/vendors/@coreui/coreui/js/coreui.bundle.min.js')}}"></script>
    <script src="{{asset('dist/vendors/simplebar/js/simplebar.min.js')}}"></script>
    <script src="{{asset('dist/parsley/parsley.js')}}"></script>
    <script src="{{asset('dist/parsley/parsley.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.10/datepicker.min.js" ></script>
    <script>
      $(document).ready(function() {
			toastr.options = {
				'closeButton': true,
				'debug': false,
				'newestOnTop': false,
				'progressBar': true,
				'positionClass': 'toast-top-right',
				'preventDuplicates': false,
				// 'showDuration': '500',
				// 'hideDuration': '1000',
				// 'timeOut': '5000000',
				// 'extendedTimeOut': '1000',
				// 'showEasing': 'swing',
				// 'hideEasing': 'linear',
				// 'showMethod': 'fadeIn',
				// 'hideMethod': 'fadeOut',
			}
		});


    var date = new Date();
    var today = new Date(date. getFullYear(), date. getMonth(), date. getDate());
    var end = new Date(date.getFullYear(), date.getMonth(), date.getDate());

      // $(function(){
      //     $("#datepicker").datepicker({
      //       minDate: 0,
      //       format: "mm-dd-yyyy",
      //       todayHighlight: true,
      //       startDate: today,
      //       endDate: end,
      //       autoclose: true

      //     });
      //     $('#datepicker').datepicker('setDate', today);
      // });
    </script>

