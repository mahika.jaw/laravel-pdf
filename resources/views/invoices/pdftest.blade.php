<style>
  *{
    margin: 0px auto;
    padding: 0px;
    box-sizing: border-box;
    font-size:20px;
}

.container{
    width: 600px;
}

.row{
    margin: 0px 0px;
}

.col-1{
    width: 8.333%;
}

.col-2{
    width: 16.666%;
}

.col-3{
    width: 25%;
}

.col-4{
    width: 33.332%;
}

.col-5{
    width: 41.665%;
}

.col-6{
    width: 50%;
}

.col-7{
    width: 58.331%;
}

.col-8{
    width: 66.664%;
}

.col-9{
    width: 75%;
}

.col-10{
    width: 83.333%;
}

.col-11{
    width: 91.663%;
}

.col-12{
    width: 100%;
}

.col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12{
    padding: 0px 0px;
}

body{
    transition: .3s;
}

.d-flex{
    display: flex;
}

.justify-space-between{
    justify-content: space-between;
}

.align-center{
    align-items: center;
}


body{
  margin-top:20px;
}

.title{
  margin-left: 0px;
}

.date{
  margin-right: 0px;
}

.table-bordered {
    border: 1px solid black;
}
.table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 20px;
}

table {
    background-color: transparent;
}

table {
    border-spacing: 0;
    border-collapse: collapse;
}



table {
    display: table;
    border-collapse: separate;
    box-sizing: border-box;
    text-indent: initial;
    border-spacing: 2px;
    border-color: gray;
}

table.dataTable {
    width: 100%;
    margin: 0 auto;
    clear: both;
    border-collapse: separate;
    border-spacing: 1px;
}

thead{
  background-color: lightgrey;
}
table.dataTable, table.dataTable th, table.dataTable td {
    box-sizing: content-box;
}

table.dataTable tbody th, table.dataTable tbody td {
    padding: 8px 10px;
}

.table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
    border: 1px solid #ddd;
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #ddd;
}
.table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #f4f4f4;
}
.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    border-top: 1px solid #f4f4f4;
}
.table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
    border: 1px solid #ddd;
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    /* border-top: 1px solid #ddd; */
}

img{
    width: 100px;
}
  
</style>

<div class="body flex-grow-1 px-3">
        <div class="container-lg">
            <div class="card mb-4">
            {{--<div class="card-header">
                    <strong>View</strong>
                    Invoice
                    <a href="{{ route('invoices.index') }}" style="float: right;" class="btn btn-info float-right ">Back</a>
                    <a href="{{ route('downloadpdf',['download'=>'pdf']) }}" style="float: right;" class="btn btn-success float-right ">Download</a>
                </div>
                <div class="card-body">--}}
                
        
                    <section class="invoice">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-xs-12">
                            <h2 class="page-header">
                                <i class="fa fa-globe"></i> Invoice #{{ $invoices->id }}
                                <small class="pull-right" style="float: right;">Invoice Dated: {{ $invoices->invoice_date }}</small>
                            </h2>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <br><br>
                        <div class="row" style="display: flex;">
                            <div class="col-sm-4 ">
                            From
                            <address>
                                <strong>{{ $invoices->created_by }}</strong><br>
                                Phone: (804) 123-5432<br>
                                Email: info@almasaeedstudio.com
                            </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 ">
                            To
                            <address>
                                <strong>{{$customer->first_name}} {{$customer->last_name}}</strong><br>
                                {{$customer->address}}<br>
                                Phone: {{$customer->phone}}<br>
                                Email: {{$customer->email}}
                            </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 ">
                            <b>Invoice No.: {{ $invoices->invoice_number }}</b>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
<br><br>
                        <!-- Table row -->
                        <div class="row">
                            <div class="col-xs-12 table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                
                                <tr>
                                <th>Qty</th>
                                <th>Product</th>
                                <th>Unit Cost</th>
                                <th>Description</th>
                                <th>Subtotal</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $a=1;?>

                                @foreach ($items as $item)
                                <tr>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{{$item->products->product_title}}</td>
                                    <td>{{ $item->cost_price }}</td>
                                    <td>{{$item->products->description}}</td>
                                    <td>{{ $item->total_price }}</td>
                                </tr>
                                
                                @endforeach
                                
                                </tbody>
                            </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <!-- accepted payments column -->
                            <div class="col-xs-6">
                            
                            
                            </div>
                            <!-- /.col -->
                            <div class="col-xs-6" style="width:40%; margin-left:616px;">
                            <p class="lead">Amount Due 2/22/2014</p>

                            <div class="table-responsive">
                                <table class="table">
                                <tr>
                                    <th style="width:50%">Subtotal:</th>
                                    <td>{{ $invoices->total_value }}</td>
                                </tr>
                                <tr>
                                    <th>Tax (9.3%)</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Shipping:</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Total:</th>
                                    <td>{{ $invoices->total_value }}</td>
                                </tr>
                                </table>
                            </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                       
                    </section>
                </div>
            </div>
    
</div>
</div>





