@extends('layouts.main')
  
@section('body')

<div class="body flex-grow-1 px-3">
        <div class="container-lg">
            <div class="card mb-4">
                <div class="card-header">
                    <strong>View</strong>
                    Invoice
                    <a href="{{ route('invoices.index') }}" style="float: right;" class="btn btn-info float-right ">Back</a>
                    <a href="{{ route('downloadpdf',['download'=>'pdf']) }}" style="float: right;" class="btn btn-success float-right ">Download</a>
                </div>
                <div class="card-body">
                
        
                    <section class="invoice">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-xs-12">
                            <h2 class="page-header">
                                <i class="fa fa-globe"></i> Invoice #{{ $invoices->id }}
                                <small class="pull-right" style="float: right;">Invoice Dated: {{ $invoices->invoice_date }}</small>
                            </h2>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <br><br>
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
                            From
                            <address>
                                <strong>{{ $invoices->created_by }}</strong><br>
                                Phone: (804) 123-5432<br>
                                Email: info@almasaeedstudio.com
                            </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                            To
                            <address>
                                <strong>{{$customer->first_name}} {{$customer->last_name}}</strong><br>
                                {{$customer->address}}<br>
                                Phone: {{$customer->phone}}<br>
                                Email: {{$customer->email}}
                            </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                            <b>Invoice No.: {{ $invoices->invoice_number }}</b>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <!-- Table row -->
                        <div class="row">
                            <div class="col-xs-12 table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                
                                <tr>
                                <th>Qty</th>
                                <th>Product</th>
                                <th>Unit Cost</th>
                                <th>Description</th>
                                <th>Subtotal</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $a=1;?>

                                @foreach ($items as $item)
                                <tr>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{{$item->products->product_title}}</td>
                                    <td>{{ $item->cost_price }}</td>
                                    <td>{{$item->products->description}}</td>
                                    <td>{{ $item->total_price }}</td>
                                </tr>
                                
                                @endforeach
                                
                                </tbody>
                            </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <!-- accepted payments column -->
                            <div class="col-xs-6">
                            
                            
                            </div>
                            <!-- /.col -->
                            <div class="col-xs-6" style="width:40%; margin-left:616px;">
                            <p class="lead">Amount Due 2/22/2014</p>

                            <div class="table-responsive">
                                <table class="table">
                                <tr>
                                    <th style="width:50%">Subtotal:</th>
                                    <td>{{ $invoices->total_value }}</td>
                                </tr>
                                <tr>
                                    <th>Tax (9.3%)</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Shipping:</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Total:</th>
                                    <td>{{ $invoices->total_value }}</td>
                                </tr>
                                </table>
                            </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                       
                    </section>
                </div>
            </div>
    
</div>
</div>




@endsection


<!-- {{$invoices}}
{{$items}}
{{$customer}}
{{$items[0]->products}}
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $invoices->total_value }}
                {{ $invoices->invoice_number }}
                {{ $invoices->invoice_date }}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            @foreach ($items as $item)
                <p>This is user {{ $item->quantity }}</p>
                <p>This is user {{ $item->cost_price }}</p>
                <p>This is user {{ $item->total_price }}</p><br><br>
                 {{$item->products->product_title}}
                
            
            @endforeach


            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Details:</strong>
                {{ $items }}
            </div>
        </div>
    </div> -->