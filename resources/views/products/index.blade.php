@extends('layouts.main')
 
@section('body')
<div class="body flex-grow-1 px-3">
        <div class="container-lg">
    <div class="row">
        <div class="col-lg-12 margin-tb">
        <div class="pull-left" style="float: left;">
                <h2>Laravel CRUD Products</h2>
            </div>
            <div class="pull-right" style="float: right;">
                <a class="btn btn-success" href="{{ route('products.create') }}"> Create New Product</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Title</th>
            <th>PRICE</th>
            <th>SKU</th>
            <th>DESCRIPTIOM</th>
            <th>CATEGORY</th>
            <th>STOCK</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($products as $product)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $product->product_title }}</td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->sku }}</td>
            <td>{{ $product->description }}</td>
            <td>{{ $product->category }}</td>
            <td>{{ $product->stock }}</td>
            <td>
                <form action="{{ route('products.destroy',$product->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('products.show',$product->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $products->links() !!}
</div>
</div>
@endsection