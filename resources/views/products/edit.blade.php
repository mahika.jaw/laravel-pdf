@extends('layouts.main')
   
@section('body')
<div class="body flex-grow-1 px-3">
        <div class="container-lg">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Product</h2>
            </div>
            <div class="pull-right" style="float: right;">
                <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('products.update',$product->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <!-- <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" value="{{ $product->name }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Detail:</strong>
                    <textarea class="form-control" style="height:150px" name="detail" placeholder="Detail">{{ $product->detail }}</textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div> -->




        <div class="row g-3">
            <div class="row g-3">
                <div class="col-sm-7">
                    <label for="basic-url" class="form-label">Product title</label>
                    <input type="text" class="form-control form-control-sm" placeholder="Product title" value="{{ $product->product_title }}" name="product_title">
                </div>
                <div class="col-sm">
                    <label for="basic-url" class="form-label">Price</label>
                    <input type="text" class="form-control form-control-sm" placeholder="Price" value="{{ $product->price }}" name="price">
                </div>
                <div class="col-sm">
                    <label for="basic-url" class="form-label">SKU</label>
                    <input type="text" class="form-control form-control-sm" placeholder="SKU" value="{{ $product->sku }}" name="sku">
                </div>
            </div>

            <div class="row g-3">
                <div class="col-sm">
                    <label for="basic-url" class="form-label">Description</label>
                    <textarea class="form-control form-control-sm" name="description" placeholder="Enter Description Here" name="description">{{ $product->description }}</textarea>
                </div>
            </div>
            <div class="row g-3">
                <div class="col-sm">
                    <label for="basic-url" class="form-label">Stock</label>
                    <input type="text" class="form-control form-control-sm" placeholder="Stock" value="{{ $product->stock }}" name="stock">
                </div>
                <div class="col-sm">
                    <label for="basic-url" class="form-label">Category</label>
                    <input type="text" class="form-control form-control-sm" placeholder="Category" value="{{ $product->category }}" name="category">
                </div>
                <div class="col-sm">
                    <label for="basic-url" class="form-label">Product Images</label>
                    <input type="file" class="form-control form-control-sm" placeholder="Upload Images" value="{{ $product->image }}" name="image">
                </div>
                
            </div>

            <div class="row g-3">
            <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </div>
        </div>
   
    </form>
</div>
</div>
@endsection