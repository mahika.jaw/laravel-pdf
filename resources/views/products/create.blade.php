@extends('layouts.main')
  
@section('body')
<div class="body flex-grow-1 px-3">
        <div class="container-lg">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left" style="float: left;">
            <h2>Add New Product</h2>
        </div>
        <div class="pull-right" style="float: right;">
            <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('products.store') }}" method="POST">
    @csrf
  
     <!-- <div class="row g-3">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" placeholder="Name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Detail:</strong>
                <textarea class="form-control" style="height:150px" name="detail" placeholder="Detail"></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div> -->
<!-- DIVIDER -->
    <div class="row g-3">
        <div class="row g-3">
            <div class="col-sm-7">
                <label for="basic-url" class="form-label">Product title</label>
                <input type="text" class="form-control form-control-sm" placeholder="Product title" name="product_title">
            </div>
            <div class="col-sm">
                <label for="basic-url" class="form-label">Price</label>
                <input type="text" class="form-control form-control-sm" placeholder="Price" name="price">
            </div>
            <div class="col-sm">
                <label for="basic-url" class="form-label">SKU</label>
                <input type="text" class="form-control form-control-sm" placeholder="SKU" name="sku">
            </div>
        </div>

        <div class="row g-3">
            <div class="col-sm">
                <label for="basic-url" class="form-label">Description</label>
                <textarea class="form-control form-control-sm" name="description" placeholder="Enter Description Here" name="description"></textarea>
            </div>
        </div>
        <div class="row g-3">
            <div class="col-sm">
                <label for="basic-url" class="form-label">Stock</label>
                <input type="text" class="form-control form-control-sm" placeholder="Stock" name="stock">
            </div>
            <div class="col-sm">
                <label for="basic-url" class="form-label">Category</label>
                <input type="text" class="form-control form-control-sm" placeholder="Category" name="category">
            </div>
            <div class="col-sm">
                <label for="basic-url" class="form-label">Product Images</label>
                <input type="file" class="form-control form-control-sm" placeholder="Upload Images" name="image">
            </div>
            
        </div>

        <div class="row g-3">
        <div class="col-xs-4">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </div>
    </div>


            <!-- <div class="form-floating">
                <input type="text" name="name" class="form-control form-control-sm" placeholder="Name">
                <label class="form-label">Name:</label>
            </div> -->
</form>
</div>
</div>
@endsection